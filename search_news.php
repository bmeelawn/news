<?php

 include 'request/search_news.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search News</title>
</head>
<body>

    <form action="" method="post">
        <input type="text" name="search">
        <input type="submit" name="search-submit" value="Search">
    </form>
    
    
   <?php
   if(count($response['data']) > 0 && $response['status'] == true) {
   foreach($response['data'] as $news=>$values) {
    ?>
    <div>
    <h3><?=$response['data'][$news]['title']?></h3>
    <h4>By <?=$response['data'][$news]['source']?><h4>
    <hr>
    <img src="<?=$response['data'][$news]['image']?>" alt="" height="400" width="700">    
    <p><?=$response['data'][$news]['description']?></p>
    <p><?=$response['data'][$news]['date']?></p>
    <hr>

</div>
      
      <?php } } else {
          echo "<h1>".$response['message']."</h1>";
      } ?>

</body>
</html>