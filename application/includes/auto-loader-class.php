<?php

spl_autoload_register('myAutoloader');

function myAutoloader($className) {
    $path = '';
    if(strripos($className, 'View')) {
        $path = '../view/';
    } elseif(strripos($className, 'contr')) {
        $path = '../controller/';
    }

    if(!empty($path)) {
        $extension = '.php';
        $fullPath = $path . lcfirst($className) . $extension;
        include $fullPath;
    }

}