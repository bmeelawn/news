<?php
header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

include '../includes/auto-loader-class.php';

// Get Body Content from request
$entityBody = json_decode(file_get_contents('php://input'), true);

$searchValue = $entityBody['searchValue'];

$news = new SearchView();
echo $news->viewAllSearchNews($searchValue);