<?php
include '../config/dbconnection.php';

class NewsModel extends DBConnection {
    protected $results;
    protected $count;

    protected function getNews() {
        $sql = "SELECT * from news";
        $this->prepare($sql);
        $this->execute();
        $this->results = $this->resultSet();
        $this->count = $this->rowCount();
    }
}