<?php
include '../config/dbconnection.php';

class SearchModel extends DBConnection {
    protected $results;
    protected $count;

    protected function getSearchNews($searchValue) {
        $sql = "SELECT * from news WHERE title like ? OR description like ?";
        $this->prepare($sql);
        $this->bindValue(1, '%'.$searchValue.'%');
        $this->bindValue(2, '%'.$searchValue.'%');
        $this->execute();
        $this->results = $this->resultSet();
        $this->count = $this->rowCount();
    }
}