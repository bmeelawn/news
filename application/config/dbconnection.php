<?php
include 'dbconfig.php';

class DBConnection {
    private $db_host = DB_HOST;
    private $db_user = DB_USER;
    private $db_pass = DB_PASS;
    private $db_name = DB_NAME;

    private $connection = false;
    private $conn;
    private $error;
    private $stmt;


    public function dbConnection() {
        // Specify DSN 
        $dsn = 'mysql:host='.$this->db_host.';dbname='.$this->db_name;
        $options = [
            PDO::ATTR_EMULATE_PREPARES => false,
             PDO::ATTR_DEFAULT_FETCH_MODE =>  PDO::FETCH_ASSOC
        ];
        // Initialize PDO DBConnection
        try {
        $this->conn = new PDO($dsn, $this->db_user, $this->db_pass, $options);
        $this->connection = true;
        } catch(Exception $e) {
            $this->error = "Database Connection Failed. " . $e->getMessage();
        }
    }

    public function dbConnected() {
        return $this->connection;
    }
    public function dbError() {
        return $this->error;
    }
    public function prepare($sql) {
        $this->stmt =$this->conn->prepare($sql);
    }
    public function bindValue($name, $value, $type = null) {
        if(is_null($type)) {
            switch(true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                break;
                case is_null($value):
                    $type = PDO::PARAM_NULl;
                break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        return $this->stmt->bindValue($name, $value, $type);
    }
    public function execute() {
        $this->stmt->execute();
    }
    public function resultSet() {
        return $this->stmt->fetchALl();
    }
    public function single() {
        return $this->stmt->fetch();
    }
    public function lastInsertId() {
        return $this->stmt->lastInsertId();
    }
    public function rowCount() {
        return $this->stmt->rowCount();
    }

}