<?php
include '../model/SearchModel.php';

class SearchView extends SearchModel {
    private $data = [];

    public function viewAllSearchNews($searchValue) {
        // Get All News
        $this->getSearchNews($searchValue);

        if($this->count > 0) {
            foreach($this->results as $key=>$values) {
                $arr = [
                    'id' => $this->results[$key]['id'],
                    'title' => $this->results[$key]['title'],
                    'source' => $this->results[$key]['source'],
                    'image' => $this->results[$key]['image'],
                    'description' => stripslashes(trim($this->results[$key]['description'])),
                    'date' => $this->results[$key]['date']
                ];
                // Push Array
                    array_push($this->data, $arr);
            }
            // Format Data Into JSON
            return json_encode([
                'status' => true,
                'message' => 'Data is found.',
                'data' => $this->data
            ]);

        } else {
            return json_encode([
                'status' => false,
                'message' => 'No Results.',
                'data' => $this->data
            ]);
        }

    }
}