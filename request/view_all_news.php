<?php

// Request API URL
$url = "http://localhost:8080/news/application/api/view_all_news.php";

// Create a session for url
$curl = curl_init($url);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json'
]);

$response = json_decode(curl_exec($curl), true);
curl_close($curl);