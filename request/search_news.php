<?php
if(isset($_POST['search-submit'])) {
    
$data =  [
    'searchValue' => $_POST['search']
]; 


// Search API URL

$url = "http://localhost:8080/news/application/api/search_news.php";

// Create a new CURL Session for Url

$curl = curl_init($url);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS,  json_encode($data));
curl_setopt($curl, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json'
]);

// Execute cUrl request with all previous settings
$response = json_decode(curl_exec($curl), true);

// Close cUrl session
curl_close($curl);

} else {
    header("location: index.php");
}