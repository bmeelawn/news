<?php
 include 'request/view_all_news.php';

 function textualDate($date) {
    $timestamp = strtotime($date);
    return date('D,M d,Y', $timestamp);
 }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>News</title>
</head>
<style type="text/css">
</style>
<body>

    <form action="search_news.php" method="post">
        <input type="text" name="search">
        <input type="submit" name="search-submit" value="Search">
    </form>
    <div class='container'>
    <div class="left-half">
   <?php
   if(count($response['data']) > 0 && $response['status'] == true) {
   foreach($response['data'] as $news=>$values) {
    ?>
    <div>
    <h3><?=$response['data'][$news]['title']?></h3>
    <h4>By <?=$response['data'][$news]['source']?><h4>
    <hr>
    <img src="<?=$response['data'][$news]['image']?>" alt="" height="400" width="700">    
    <p><?=$response['data'][$news]['description']?></p>
    <p><?=textualDate($response['data'][$news]['date'])?></p>
    <hr>

</div>
      
      <?php } } else {
          echo "<h1>".$response['message']."</h1>";
      }
          ?>
          </div>
          
          <div class="right-half">
          <h3>Headline</h3>
          <?php
   if(count($response['data']) > 0 && $response['status'] == true) {
   foreach($response['data'] as $news=>$values) {
    ?>
    <div>
    <h3><?=$response['data'][$news]['title']?></h3>
</div>
      
      <?php } } else {
          echo "<h1>".$response['message']."</h1>";
      }
          ?>
          </div>
          </div>
          <div>

</body>
</html>